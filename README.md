# SWA Bazaar BazarService


## How to run

1. Clone repository
2. Open folder in cmd
3. `docker-compose pull`
4. `sh fix-line-endings.sh` [see J. Lhotak's Service](https://gitlab.fel.cvut.cz/lhotajak/swa-bazaar-message-service)
5. `docker-compose up -d`

## Swagger 
http://localhost:8082/swagger-ui/index.html#/

## Ports
 - :5433 - Postgres DB
 - :8082 - Bazar service


## Docker Hub

https://hub.docker.com/repository/docker/hromato9/fel-swa-bazar-service

## Interesting Links

 - [Deploying Image to Docker Hub](https://dev.to/mattdark/publish-a-docker-image-to-docker-hub-using-gitlab-2b9o)
 - https://docs.docker.com/compose/reference/

## ENV file
 - `LOGIN_SERVICE_PATH` url for login service 
 - `LOGIN_CHECK`  if `false` tokens are not checked and login service is not required