package cz.swa.bazar.bazar_service.authentication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AuthenticationConfig {

    @Value("${login.service.path}")
    private String loginServerPath;

    @Value("${login.check}")
    private boolean checkToken;

    @Bean
    public WebClient loginApiClient() {
        if(!checkToken){
            return WebClient.create();
        }
        return WebClient.create(loginServerPath);
    }
}
