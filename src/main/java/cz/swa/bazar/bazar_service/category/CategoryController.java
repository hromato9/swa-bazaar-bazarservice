package cz.swa.bazar.bazar_service.category;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "api/categories")
@Api(tags="Categories Controller")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation(value = "Get all categories", notes = "Returns all categories name and their ids")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved")
    })
    @GetMapping
    public List<Category> getCategories(){
        return categoryService.getAllCategories();
    }

    @ApiOperation(value = "Get category by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Category found successfully"),
            @ApiResponse(code = 404, message = "Category not found")
    })
    @GetMapping(path = "{categoryId}")
    public Category getCategory(@PathVariable("categoryId") @ApiParam(name = "categoryId", value = "category ID", example = "1") Long categoryId){
        return categoryService.getById(categoryId);
    }
}
