package cz.swa.bazar.bazar_service;

import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryService;
import cz.swa.bazar.bazar_service.item.Item;
import cz.swa.bazar.bazar_service.item.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PatchItemTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemService itemService;

    @Autowired
    private CategoryService categoryService;

    private static String patchString(String parameter, String newValue){
        return String.format("[{ \"op\": \"replace\", \"path\": \"/%s\", \"value\": \"%s\" }]",
                parameter, newValue);
    }

    private static String patchStringJson(String parameter, String newValue){
        return String.format("[{ \"op\": \"replace\", \"path\": \"/%s\", \"value\": %s }]",
                parameter, newValue);
    }

    private static String patchAddString(String parameter, String newValue){
        return String.format("[{ \"op\": \"add\", \"path\": \"/%s\", \"value\": \"%s\" }]",
                parameter, newValue);
    }

    @Test
    public void patchItemPrice() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("price", "20000");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(200))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(20000.00f, patchItem.getPrice().floatValue(), 0.001f);
    }

    @Test
    public void patchName() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("name", "HP laptop");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(200))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals("HP laptop", patchItem.getName());
    }

    @Test
    public void patchDescription() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("description", "new description");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(200))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals("new description", patchItem.getDescription());
    }

    @Test
    public void patchId() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("id", "2");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(400))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(id, patchItem.getId());
    }

    @Test
    public void patchCategory() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();

        Category cat2 = categoryService.getById(2L);
        String param = patchStringJson("category", "{\"id\": 2}");

        mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(200))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(cat2.getId(), patchItem.getCategory().getId());
    }

    @Test
    public void patchUserId() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();

        String param = patchString("userId", "5");

        mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(200))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(5L, patchItem.getUserId());
    }

    @Test
    public void patchNegativePrice() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("price", "-200");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(400))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(14552.f, patchItem.getPrice().floatValue(), 0.001f);
    }

    @Test
    public void patchNotExistingItem() throws Exception {
        String param = patchString("price", "20000");

        mockMvc.perform(
                        patch("/api/items/" + 74444)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(404))
                .andReturn();
    }

    @Test
    public void patchItemNotExistingParam() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();

        String param = patchString("surname", "20000");
        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void patchItemPriceWithText() throws Exception {
        Category cat = categoryService.getById(1L);
        Item item = new Item("Laptop", "Working laptop", BigDecimal.valueOf(14552),2L, cat);
        long id = itemService.addNewItem(item).getId();
        String param = patchString("price", "awra");

        MvcResult result = mockMvc.perform(
                        patch("/api/items/" + id)
                                .header("Content-Type", "application/json")
                                .content(param)
                ).andDo(print())
                .andExpect(status().is(400))
                .andReturn();

        Item patchItem = itemService.getItemById(id);
        assertEquals(14552, patchItem.getPrice().floatValue(), 0.001f);
    }
}
