package cz.swa.bazar.bazar_service.item;

import cz.swa.bazar.bazar_service.authentication.AuthenticationService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.json.JsonPatch;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(path = "api/items")
@Api(
        tags="Items Controller")
public class ItemController {

    private final ItemService itemService;
    private final AuthenticationService authenticationService;

    @Value("${login.check}")
    private boolean checkToken;

    @Autowired
    public ItemController(ItemService itemService,
                          AuthenticationService authenticationService) {
        this.itemService = itemService;
        this.authenticationService = authenticationService;
    }

    @ApiOperation(value = "Get all items", notes = "Returns all items. Using parameters the items can be filtered and ordered." +
            " For example by category or price.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved"),
        @ApiResponse(code = 400, message = "Bad request: Invalid argument value for order by parameter")
    })
    @GetMapping(produces = "application/json")
    public List<Item> getItems(@RequestParam(value = "name of item", required = false) String name,
                               @RequestParam(value = "category", required = false) Long categoryId,
                               @RequestParam(value = "user id", required = false) Long userId,
                               @RequestParam(value = "min price", required = false) BigDecimal minPrice,
                               @RequestParam(value = "max price", required = false) BigDecimal maxPrice,
                               @RequestParam(value = "order by (0=update_time, 1=price)", required = false, defaultValue = "0") Integer orderBy,
                               @RequestParam(value = "desc", required = false, defaultValue = "true") Boolean descending,
                               @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>", required = false) String authHeader){
        if(checkToken){
           authenticationService.validateToken(getToken(authHeader));
        }
        return itemService.searchItem(name, categoryId, userId, minPrice, maxPrice, orderBy, descending);
    }

    @ApiOperation(value = "Add a new item")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item successfully added"),
            @ApiResponse(code = 400, message = "Bad request, item is not defined correctly"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "item",
                    dataTypeClass = Item.class,
                    example = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": 2550.20, \"userId\": 2}"
            )
    })
    @PostMapping
    public Item addNewItem(@RequestBody Item item,
                           @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>", required = false) String authHeader){

        if(checkToken){
            Long userId = authenticationService.validateToken(getToken(authHeader));
            if(item.getUserId() != userId){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not valid user");
            }
        }
        return itemService.addNewItem(item);
    }

    @ApiOperation(value = "Delete an item by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item successfully deleted"),
            @ApiResponse(code = 404, message = "Not found - The item was not found")
    })
    @DeleteMapping(path = "{itemId}")
    public void deleteItem(@PathVariable("itemId") @ApiParam(name = "itemId", value = "Item id", example = "1") Long itemId,
                           @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>", required = false) String authHeader){
        if(checkToken){
            Long userId = authenticationService.validateToken(getToken(authHeader));
            if(userId != itemService.getItemUserId(itemId)){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not valid user");
            }
        }
        itemService.deleteItem(itemId);
    }

    @ApiOperation(value = "Get item with specific id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item successfully found"),
            @ApiResponse(code = 404, message = "Not found - The item was not found")
    })
    @GetMapping(path = "{itemId}")
    public Item getItem(@PathVariable("itemId") @ApiParam(name = "itemId", value = "Item id", example = "1") Long itemId, @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>", required = false) String authHeader){
        if(checkToken) {
            authenticationService.validateToken(getToken(authHeader));
        }
        return itemService.getItemById(itemId);
    }

    @PatchMapping(path = "{itemId}", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item has been updated"),
            @ApiResponse(code = 400, message = "Request body has invalid format"),
            @ApiResponse(code = 404, message = "Item not found")
    })
    @ApiOperation(value = "Update item values")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "itemId", value = "1", required = true),
        @ApiImplicitParam(
                name = "patchDocument",
                dataTypeClass = JsonPatch.class,
                example = "[{ \"op\": \"replace\", \"path\": \"/name\", \"value\": \"New name\" }]"
        )
    })
    @ResponseStatus(HttpStatus.OK)
    public void patchItem(@PathVariable("itemId") @ApiParam(value = "Item id", example = "1", required = true) Long itemId,
                          @RequestBody JsonPatch patchDocument, @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>", required = false) String authHeader){
        if(checkToken){
            Long userId = authenticationService.validateToken(getToken(authHeader));
            if(userId != itemService.getItemUserId(itemId)){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Not valid user");
            }
        }
        itemService.patchItem(itemId, patchDocument);
    }

    public static String getToken(String authHeader){
        if(!authHeader.startsWith("Bearer ")){
            return null;
        }
        return authHeader.substring("Bearer ".length());
    }
}
