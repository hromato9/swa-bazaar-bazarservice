package cz.swa.bazar.bazar_service.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.Exceptions;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class AuthenticationService {

    private static final Duration REQUEST_TIMEOUT = Duration.ofSeconds(3);

    private WebClient loginApiClient;

    @Autowired
    public AuthenticationService(WebClient loginApiClient){
        this.loginApiClient = loginApiClient;
    }

    public Long validateToken(String token){
        String json = loginApiClient.get().uri("/" + token)
                .retrieve()
                .onStatus(httpStatus -> httpStatus.value() == 422, response -> {
                    throw Exceptions.propagate(
                    new ResponseStatusException(HttpStatus.FORBIDDEN, "Token not found"));
                })
                .bodyToMono(String.class)
                .block();

        return Long.valueOf(json.replace("\"",""));
    }
}
