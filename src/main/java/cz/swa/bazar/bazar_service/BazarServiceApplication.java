package cz.swa.bazar.bazar_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BazarServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BazarServiceApplication.class, args);
	}
}
