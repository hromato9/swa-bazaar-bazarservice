package cz.swa.bazar.bazar_service.item;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr353.JSR353Module;
import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Configuration
public class ItemConfig {

    @Bean
    CommandLineRunner commandLineRunner(ItemRepository repository, CategoryRepository catRepository) {
        return args -> {
            Category cat1 = new Category("Electronics");
            Category cat2 = new Category( "Clothes");
            Category cat3 = new Category( "Books");
            List<Category> cats = catRepository.saveAll(List.of(cat1, cat2, cat3));
            catRepository.flush();
            cat1 = cats.get(2);
            cat2 = cats.get(1);
            cat3 = cats.get(0);
            repository.saveAll(
                    List.of(
                            new Item("Tv", "Description", new BigDecimal(2000), LocalDate.now(), LocalDate.now(), 3L, cat2),
                            new Item("Phone Sony", "Description", new BigDecimal(1500), LocalDate.now(), LocalDate.now(),  3L, cat1),
                            new Item("Samsung Phone", "Description", new BigDecimal(1800), LocalDate.now(), LocalDate.now(),  3L, cat1),
                            new Item("Tshirt", "A good red t shirt", new BigDecimal(300), LocalDate.now(), LocalDate.now(),  1L, cat2),
                            new Item("Harry Potter", "A book", new BigDecimal(100), LocalDate.now(), LocalDate.now(),  2L, cat3)
                    )
            );
        };
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
                .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .findAndRegisterModules();
    }
}
