package cz.swa.bazar.bazar_service.item;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.swa.bazar.bazar_service.category.Category;
import io.swagger.annotations.ApiModelProperty;
import jdk.jfr.Enabled;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table
public class Item {
    @Id
    @SequenceGenerator(
            name = "item_sequence",
            sequenceName = "item_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "item_sequence"
    )
    @ApiModelProperty(notes = "Item ID", example = "1", required = true)
    private Long id;
    @Column(nullable = false)
    @ApiModelProperty(notes = "Item name", example = "Samsung Galaxy S5", required = true)
    private String name;
    @Column(nullable = false)
    @ApiModelProperty(notes = "Item description", example = "Used phone but works fine.", required = true)
    private String description;
    @Column(nullable = false)
    @ApiModelProperty(notes = "Item price", example = "2520.50", required = true)
    private BigDecimal price;
    private LocalDate created;
    private LocalDate updated;
    @Column(nullable = false)
    @ApiModelProperty(notes = "User ID of the seller", example = "1", required = true)
    private Long userId;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="category_id", nullable=false, referencedColumnName = "id")
    @JsonIgnoreProperties({"name"})
    private Category category;

    public Item(){

    }

    public Item(Long id, String name, String description, BigDecimal price, LocalDate created, LocalDate updated,
                Long userId, Category category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.created = created;
        this.updated = updated;
        this.userId = userId;
        this.category = category;
    }

    public Item(String name, String description, BigDecimal price, LocalDate created, LocalDate updated, Long userId, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.created = created;
        this.updated = updated;
        this.userId = userId;
        this.category = category;
    }

    public Item(String name, String description, BigDecimal price, Long userId, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.created = LocalDate.now();
        this.updated = LocalDate.now();
        this.userId = userId;
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDate getCreated() {
        return created;
    }

    public LocalDate getUpdated() {
        return updated;
    }

    public Long getUserId() {
        return userId;
    }

    public Category getCategory() {
        return category;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public void setUpdated(LocalDate updated) {
        this.updated = updated;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", created=" + created +
                ", updated=" + updated +
                ", userId=" + userId +
                ", category='" + category + '\'' +
                '}';
    }
}
