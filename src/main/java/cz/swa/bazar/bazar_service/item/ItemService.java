package cz.swa.bazar.bazar_service.item;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.json.JsonException;
import javax.json.JsonPatch;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Service
public class ItemService {

    private final ItemRepository itemRepository;
    private final ObjectMapper objectMapper;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository,
                       ObjectMapper objectMapper,
                       CategoryRepository categoryRepository) {
        this.itemRepository = itemRepository;
        this.objectMapper = objectMapper;
        this.categoryRepository = categoryRepository;
    }

    public List<Item> getItems(){
        return itemRepository.findAll();
    }

    public List<Item> getItemsByCategory(String category){
        return itemRepository.findItemByCategory(category);
    }

    public Item addNewItem(Item item) {
        if(item.getId() != null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Cannot specify the id.");
        }
        if(item.getCategory() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing category.");
        }
        if(item.getName() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing name.");
        }
        if(item.getDescription() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing description.");
        }
        if(item.getUserId() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing user id.");
        }
        if(item.getPrice() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Missing price.");
        }
        if(item.getPrice().compareTo(BigDecimal.ZERO) < 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid item price '" +
                    item.getPrice().toPlainString() + "'. Price cannot be negative. ");
        }
        if(!categoryRepository.existsById(item.getCategory().getId())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: Category '" + item.getCategory().getId() + "' does not exist.");
        }

        item.setUpdated(LocalDate.now());
        item.setCreated(LocalDate.now());
        itemRepository.save(item);
        return item;
    }

    public void deleteItem(Long itemId) {
        boolean exists = itemRepository.existsById(itemId);
        if(!exists){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "item with id " + itemId + " does not exist");
        }
        itemRepository.deleteById(itemId);
    }

    public List<Item> getItemsByUserId(Long userId) {
        return itemRepository.findItemByUserId(userId);
    }

    public Item patchItem(Long itemId, JsonPatch patchDocument) {
        boolean exists = itemRepository.existsById(itemId);
        if(!exists){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "item with id " + itemId + " does not exist");
        }
        Item item = itemRepository.getById(itemId);
        JsonStructure target = null;
        try {
            target = objectMapper.convertValue(item, JsonStructure.class);
        }catch (JsonException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: cannot patch: '" + exception.getMessage() + "'.");
        }
        Item patchItem = null;
        try {
            JsonValue patched = patchDocument.apply(target);
            patchItem = objectMapper.convertValue(patched, Item.class);
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request: cannot patch: '" + exception.getMessage() + "'.");
        }
        if(patchItem.getPrice().compareTo(BigDecimal.ZERO) < 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot patch price to negative:  " +
                    patchItem.getPrice().toPlainString() + ".");
        }
        if(patchItem.getId() != item.getId()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot change item id!");
        }
        patchItem.setCreated(item.getCreated());
        patchItem.setUpdated(LocalDate.now());
        itemRepository.save(patchItem);
        return patchItem;
    }

    public Item getItemById(Long itemId) {
        boolean exists = itemRepository.existsById(itemId);
        if(!exists){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "item with id " + itemId + " does not exist");
        }
        return itemRepository.getById(itemId);
    }

    public List<Item> searchItem(String name, Long category, Long userId,
                                 BigDecimal minPrice, BigDecimal maxPrice, Integer orderBy, Boolean desc){

        if(name == null) name = "";
        boolean orderByUpdated = true;
        if(orderBy != null){
            if(orderBy == 0) {
                orderByUpdated = true;
            }else if(orderBy == 1){
                orderByUpdated = false;
            }else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Invalid value of 'order by', allows only {0, 1}. Not " + orderBy + ".");
            }
        }
        if(desc != null && !desc){
            if(orderByUpdated){
                return itemRepository.searchForItemsUpdatedASC(name, category, userId, minPrice, maxPrice);
            }
            return itemRepository.searchForItemsPriceASC(name, category, userId, minPrice, maxPrice);
        }
        if(orderByUpdated){
            return itemRepository.searchForItemsUpdatedDESC(name, category, userId, minPrice, maxPrice);
        }
        return itemRepository.searchForItemsPriceDESC(name, category, userId, minPrice, maxPrice);
    }

    public void deleteAllItems() {
        itemRepository.deleteAll();
    }

    public Long getItemUserId(Long itemId){
        boolean exists = itemRepository.existsById(itemId);
        if(!exists){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "item with id " + itemId + " does not exist");
        }
        return itemRepository.getById(itemId).getId();
    }
}
