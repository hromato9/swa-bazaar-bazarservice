package cz.swa.bazar.bazar_service;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.swa.bazar.bazar_service.category.CategoryService;
import cz.swa.bazar.bazar_service.item.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AddItemTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ItemService itemService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void addItemValid() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": 2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        MvcResult result = mockMvc.perform(
                post("/api/items")
                        .header("Content-Type", "application/json")
                        .content(json)
                )
                .andExpect(status().isOk())
                .andReturn();
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems + 1, numberOfItemsAfter);
    }

    @Test
    public void addItemInvalidPrice() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": -2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemMissingName() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\",  \"price\": 2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemMissingDescription() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"name\": \"RC car\", \"price\": 2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemMissingCategory() throws Exception {
        String json = "{  \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": 2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemMissingUserId() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": 2550.20}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemMissingPrice() throws Exception {
        String json = "{ \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\",  \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemSettingOwnId() throws Exception {
        String json = "{\"id\": 1, \"category\": { \"id\": 1}, \"description\": \"a brand new car\", \"name\": \"RC car\",  \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
        mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                )
                .andExpect(status().is(400));
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

    @Test
    public void addItemNotExistingCategory() throws Exception {
        String json = "{ \"category\": { \"id\": 10000}, \"description\": \"a brand new car\", \"name\": \"RC car\", \"price\": 2550.20, \"userId\": 2}";
        int numberOfItems = itemService.getItems().size();
       mockMvc.perform(
                        post("/api/items")
                                .header("Content-Type", "application/json")
                                .content(json)
                ).andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(400)).andReturn();
        int numberOfItemsAfter = itemService.getItems().size();
        assertEquals(numberOfItems, numberOfItemsAfter);
    }

}
