package cz.swa.bazar.bazar_service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryService;
import cz.swa.bazar.bazar_service.item.Item;
import cz.swa.bazar.bazar_service.item.ItemService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SearchItemTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ItemService itemService;

    @Autowired
    private CategoryService categoryService;

    @BeforeEach
    public void initData(){
        itemService.deleteAllItems();
        Category cat1 = categoryService.getById(3L);
        Category cat2 = categoryService.getById(2L);
        Category cat3 = categoryService.getById(1L);
        List<Item> items = List.of(
                new Item( "Tv", "Description", new BigDecimal(2000),3L, cat2),
                new Item("Phone Sony", "Description", new BigDecimal(1500),3L, cat1),
                new Item("Samsung Phone", "Description", new BigDecimal(1800), 3L, cat1),
                new Item("Tshirt", "A good red t shirt", new BigDecimal(300),1L, cat2),
                new Item("Harry Potter", "A book", new BigDecimal(100), 2L, cat3)
        );
        for(Item item : items){
            itemService.addNewItem(item);
        }
    }

    @Test
    public void searchWithoutParameters() {
        List<Item> res = itemService.searchItem(null, null, null, null, null, null, null);
        assertEquals(5, res.size());
    }

    @Test
    public void searchByName() {
        List<Item> res = itemService.searchItem("Ph", null, null, null, null, null, null);
        assertEquals(2, res.size());
    }
    @Test
    public void searchByNameAndUserNegative() {
        List<Item> res = itemService.searchItem("Ph", null, 1L, null, null, null, null);
        assertEquals(0, res.size());
    }
    @Test
    public void searchByNameAndUserPositive() {
        List<Item> res = itemService.searchItem("Ph", null, 3L, null, null, null, null);
        assertEquals(2, res.size());
    }
    @Test
    public void searchByUser() {
        List<Item> res = itemService.searchItem(null, null, 3L, null, null, null, null);
        assertEquals(3, res.size());
    }
    @Test
    public void searchByCategory() {
        List<Item> res = itemService.searchItem(null, 1L, null, null, null, null, null);
        assertEquals(1, res.size());
    }

    @Test
    public void searchByMinPrice() {
        List<Item> res = itemService.searchItem(null, null, null, BigDecimal.valueOf(300), null, null, null);
        assertEquals(4, res.size());
    }

    @Test
    public void searchByMaxPrice() {
        List<Item> res = itemService.searchItem(null, null, null, null, BigDecimal.valueOf(500), null, null);
        assertEquals(2, res.size());
    }

    @Test
    public void searchByMinAndMaxPrice() {
        List<Item> res = itemService.searchItem(null, null, null, BigDecimal.ZERO, BigDecimal.valueOf(1500), null, null);
        assertEquals(3, res.size());
    }

    @Test
    public void searchByCategoryMinAndMaxPrice() {
        List<Item> res = itemService.searchItem(null, 2L, null, BigDecimal.ZERO, BigDecimal.valueOf(300), null, null);
        assertEquals(1, res.size());
        Item item = res.get(0);
        assertEquals(2L, item.getCategory().getId());
        assertTrue(300 <= item.getPrice().doubleValue());
        assertTrue(item.getPrice().doubleValue() <= 1500);
    }
    @Test
    public void searchAllFilterByPriceDesc() {
        List<Item> res = itemService.searchItem(null, null, null, null, null, 1, true);
        BigDecimal price = BigDecimal.valueOf(Float.MAX_VALUE);
        for(Item item : res){
            assertTrue(price.doubleValue() >= item.getPrice().doubleValue());
            price = item.getPrice();
        }
    }
    @Test
    public void searchAllFilterByPriceAsc() {
        List<Item> res = itemService.searchItem(null, null, null, null, null, 1, false);
        BigDecimal price = BigDecimal.ZERO;
        for(Item item : res){
            assertTrue(price.doubleValue() <= item.getPrice().doubleValue());
            price = item.getPrice();
        }
    }

    @Test
    public void searchAllFilterByUpdateDesc() {
        List<Item> res = itemService.searchItem(null, null, null, null, null, 0, true);
        LocalDate loc = LocalDate.of(3000, 1, 1);
        for(Item item : res){
            assertTrue(loc.compareTo(item.getUpdated()) >= 0);
            loc = item.getUpdated();
        }
    }

    @Test
    public void searchAllFilterByUpdateAsc() {
        List<Item> res = itemService.searchItem(null, null, null, null, null, 0, false);
        LocalDate loc = LocalDate.of(0, 1, 1);
        for(Item item : res){
            assertTrue(loc.compareTo(item.getUpdated()) <= 0);
            loc = item.getUpdated();
        }
    }

    @Test
    public void getItemByIdNotExisting(){
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            itemService.getItemById(1000L);
        });
        assertEquals(404, exception.getStatus().value());
    }
}
