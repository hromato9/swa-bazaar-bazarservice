package cz.swa.bazar.bazar_service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryService;
import cz.swa.bazar.bazar_service.item.ItemService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CategoryTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void showAllCategories(){
        List<Category> cats = categoryService.getAllCategories();
        assertEquals(3, cats.size());

        List<String> categories = new ArrayList<>();
        for(Category category : cats){
            categories.add(category.getName());
        }
        List<String> expected = new ArrayList<>();
        expected.add("Electronics");
        expected.add("Clothes");
        expected.add("Books");

        assertEquals(expected, categories);
    }

    @Test
    public void getCategoryById(){
        Long id = categoryService.getAllCategories().get(0).getId();
        String name = categoryService.getById(id).getName();
        assertEquals("Electronics", name);
    }
    @Test
    public void getCategoryByIdNotExisting(){
        ResponseStatusException exception = assertThrows(ResponseStatusException.class, () -> {
            categoryService.getById(1000L);
        });
        assertEquals(404, exception.getStatus().value());
    }

}
