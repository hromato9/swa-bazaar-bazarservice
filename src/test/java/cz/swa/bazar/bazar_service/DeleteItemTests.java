package cz.swa.bazar.bazar_service;

import cz.swa.bazar.bazar_service.category.Category;
import cz.swa.bazar.bazar_service.category.CategoryService;
import cz.swa.bazar.bazar_service.item.Item;
import cz.swa.bazar.bazar_service.item.ItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class DeleteItemTests {

    @Autowired
    private ItemService itemService;

    @Autowired
    private CategoryService categoryService;

    @Test
    public void deleteItem(){
        //Given
        Category category = categoryService.getById(1L);
        Item item = new Item("Phone", "Description", BigDecimal.ZERO, 1L, category);
        long id = itemService.addNewItem(item).getId();

        int count = itemService.getItems().size();
        itemService.deleteItem(id);
        assertThrows(ResponseStatusException.class, () -> {
            itemService.getItemById(id);
        });
        int countAfter = itemService.getItems().size();
        assertEquals(count - 1, countAfter);
    }

    @Test
    public void deleteItemNotExists(){
        int count = itemService.getItems().size();
        assertThrows(ResponseStatusException.class, () -> {
            itemService.deleteItem(1555L);
        });
        int countAfter = itemService.getItems().size();
        assertEquals(count, countAfter);

    }
}
