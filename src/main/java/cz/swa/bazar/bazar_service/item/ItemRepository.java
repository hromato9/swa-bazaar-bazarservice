package cz.swa.bazar.bazar_service.item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    //@Query("SELECT i FROM Item i WHERE i.userId = ?1")
    List<Item> findItemByUserId(Long userId);

    List<Item> findItemByCategory(String category);

    //
    // AND (:user = null OR i.userId = :user) , @Param("user") Long userId) @Param("name") String name,
    @Query("SELECT i FROM Item i WHERE ((lower(i.name) LIKE concat('%', lower(:name),'%')) AND (:cat is null OR i.category.id = :cat) AND (:user is null OR i.userId = :user) " +
            "AND (:minp is NULL OR i.price >= :minp) AND (:maxp is NULL OR i.price <= :maxp)) ORDER BY i.updated ASC") //
    List<Item> searchForItemsUpdatedASC(@Param("name") String name, @Param("cat") Long cat, @Param("user") Long userId, @Param("minp") BigDecimal minp, @Param("maxp") BigDecimal maxp);

    @Query("SELECT i FROM Item i WHERE ((lower(i.name) LIKE concat('%', lower(:name),'%')) AND (:cat is null OR i.category.id = :cat) AND (:user is null OR i.userId = :user) " +
            "AND (:minp is NULL OR i.price >= :minp) AND (:maxp is NULL OR i.price <= :maxp)) ORDER BY i.updated DESC") //
    List<Item> searchForItemsUpdatedDESC(@Param("name") String name, @Param("cat") Long cat, @Param("user") Long userId, @Param("minp") BigDecimal minp, @Param("maxp") BigDecimal maxp);

    @Query("SELECT i FROM Item i WHERE ((lower(i.name) LIKE concat('%', lower(:name),'%')) AND (:cat is null OR i.category.id = :cat) AND (:user is null OR i.userId = :user) " +
            "AND (:minp is NULL OR i.price >= :minp) AND (:maxp is NULL OR i.price <= :maxp)) ORDER BY i.price DESC") //
    List<Item> searchForItemsPriceDESC(@Param("name") String name, @Param("cat") Long cat, @Param("user") Long userId, @Param("minp") BigDecimal minp, @Param("maxp") BigDecimal maxp);

    @Query("SELECT i FROM Item i WHERE ((lower(i.name) LIKE concat('%', lower(:name),'%')) AND (:cat is null OR i.category.id = :cat) AND (:user is null OR i.userId = :user) " +
            "AND (:minp is NULL OR i.price >= :minp) AND (:maxp is NULL OR i.price <= :maxp)) ORDER BY i.price ASC") //
    List<Item> searchForItemsPriceASC(@Param("name") String name, @Param("cat") Long cat, @Param("user") Long userId, @Param("minp") BigDecimal minp, @Param("maxp") BigDecimal maxp);
}
