package cz.swa.bazar.bazar_service.category;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table
public class Category {

    @Id
    @SequenceGenerator(
            name = "category_sequence",
            sequenceName = "category_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "category_sequence"
    )
    @ApiModelProperty(notes = "Category ID", example = "1", required = true)
    private Long id;
    @ApiModelProperty(notes = "Category Name", example = "Phones", required = true)
    private String name;

    public Category(){}

    public Category(String name){
        this.name = name;
    }

    public Category(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
